/* See LICENSE file for copyright and license details.*/

#ifndef CONFIG_H
#define CONFIG_H

static Task config_tasks[] = {
/*       name,             day,    month,  start(hour, min) duration */
	{"fixed0",         e_day,  e_mon,  {5, 16},         30},
	{"task0",          e_day,  e_mon,  {after_last},    dyn},
	{"fixed1",         e_day,  e_mon,  {8, 0},          40},
	{"task1",          e_day,  e_mon,  {after_last},    30},
	{"task2",          e_day,  e_mon,  {after_last},    60},
	{"task3",          e_day,  e_mon,  {after_last},    10},
	{"task4",          e_day,  e_mon,  {after_last},    60},
	{"task5",          e_day,  e_mon,  {after_last},    10},
	{"task6",          e_day,  e_mon,  {after_last},    15},
	{"task7",          Sat,    e_mon,  {after_last},    15},
	{"task8",          Sat,    e_mon,  {after_last},    dyn},
	{"task9",          Sat,    e_mon,  {10,0},          60},
	{"task10",         e_day,  e_mon,  {after_last},    dyn},
	{"fixed3",         e_day,  e_mon,  {12,30},         30},

	{"task11",         e_day,  e_mon,  {after_last},    50},
	{"task12",         e_day,  e_mon,  {after_last},    24},
	{"task13",         e_day,  e_mon,  {after_last},    30},
	{"task14",         e_day,  e_mon,  {after_last},    50},
	{"task15",         e_day,  e_mon,  {after_last},    10},
	{"task16",         e_day,  e_mon,  {after_last},    dyn},
	{"fixed4",         e_day,  e_mon,  {15,54},         30},

	{"task17",         e_day,  e_mon,  {after_last},    30},
	{"task18",         e_day,  e_mon,  {after_last},    24},
	{"task19",         e_day,  e_mon,  {after_last},    30},
	{"task20",         e_day,  e_mon,  {after_last},    30},
	{"task21",         e_day,  e_mon,  {after_last},    dyn},
	{"fixed5",         e_day,  e_mon,  {18,30},         30},

	{"task22",         e_day,  e_mon,  {after_last},    10},
	{"task23",         e_day,  e_mon,  {after_last},    20},
	{"task24",         e_day,  e_mon,  {after_last},    20},
	{"task25",         e_day,  e_mon,  {after_last},    dyn},
	{"fixed6",         e_day,  e_mon,  {20,00},         30},

	{"task26",         e_day,  e_mon,  {after_last},    dyn},
	{"fixed7",         e_day,  e_mon,  {22, 0},         0},
};

#endif /* CONFIG_H */
