splanner
========
splanner is a simple personal planner.

Installation
------------
```sh
$ git clone https://github.com/afify/splanner.git
$ cd splanner/
$ make
# make install
```
Run
---
```sh
$ splanner
```
Options
-------
```sh
$ splanner [-AaNnv]
$ man splanner
```
| option | description                                              |
|:------:|:---------------------------------------------------------|
| `-A`   | print all tasks, 12-hour clock format.                   |
| `-a`   | print all tasks.                                         |
| `-N`   | print all upcoming tasks, 12-hour clock format.          |
| `-n`   | print all upcoming tasks.                                |
| `-v`   | print version.                                           |

Configuration
-------------
The configuration of splanner is done by creating a custom config.h
and (re)compiling the source code.

Philosophy & Code Style
-----------------------
- [Philosophy].
- [Code Style].

Copyright and License
---------------------
splanner is provided under the MIT license.

[Philosophy]: <https://suckless.org/philosophy/>
[Code Style]: <https://suckless.org/coding_style/>
