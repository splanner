/*
* See LICENSE file for copyright and license details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* typedef */
enum { after_last = 25, dyn = -1 };
enum week { Sun, Mon, Tue, Wed, Thu, Fri, Sat, e_day };
enum year { Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec, e_mon };

typedef struct {
	char name[256];
	enum week day;
	enum year mon;
	int st[2];
	int dur;
} Task;

typedef struct {
	char name[256];
	time_t st;
	time_t end;
} Task_t;

#include "config.h"
#include "util.h"

/* function declarations */
static void usage(void);
static char *convert_timet_to_str(const time_t, const char);
static time_t convert_hour_min_timet(const int*);
static time_t adj_st(const int*, const time_t);
static time_t adj_end(const time_t, int);
static int accepted_task(Task);
static void gather_info(size_t*, size_t*, size_t);
static Task *create_acct(size_t, size_t*, size_t);
static Task_t *create_rett(Task*, size_t*);
static int validate_fixed(size_t*, size_t*, Task_t*, Task*);
static int validate_dyn(size_t*, Task_t*, Task*);
static void print_t(const char);

/* function implementations */
static void
usage(void)
{
	die("usage: splanner -[AaNnv]\noptions:\n    \
-A  print all tasks, 12-hour clock format.\n    \
-a  print all tasks.\n    \
-N  print all upcoming tasks, 12-hour clock format.\n    \
-n  print all upcoming tasks.\n    \
-v  print version.");
}

static char *
convert_timet_to_str(const time_t input_time, const char option)
{
	char *result;
	size_t result_size = (size_t)32;
	struct tm new_time = *localtime(&input_time);

	result = ecalloc(result_size, sizeof(char));
	if ((option == 'A' || option == 'N') && (new_time.tm_hour > 12)){
		new_time.tm_hour = new_time.tm_hour - 12;
	}
	(void)snprintf(result, result_size, "%.2d:%.2d",
		new_time.tm_hour, new_time.tm_min);
	return result;
}

static time_t
convert_hour_min_timet(const int *arr)
{
	const int hours = arr[0];
	const int minutes = arr[1];
	time_t now;
	time_t converted;
	struct tm tmnow;

	(void)time(&now);
	tmnow = *localtime(&now);
	tmnow.tm_hour = hours;
	tmnow.tm_min = minutes;
	tmnow.tm_sec = 0;
	converted = mktime(&tmnow);
	return converted;
}

static time_t
adj_st(const int *config_start_time_array, time_t last_task_endtime)
{
	time_t result;
	int config_hours = config_start_time_array[0];
	/* after_last */
	if (config_hours == (int)after_last) {
		result = last_task_endtime;
	/* fixed start */
	} else {
		result = convert_hour_min_timet(config_start_time_array);
	}
	return result;
}

static time_t
adj_end(const time_t start_time, int duration)
{
	time_t returned_time;
	duration = duration * 60;
	returned_time =  start_time + duration;
	return returned_time;
}

static int
accepted_task(Task tested_task)
{
	const time_t current_time = time(NULL);
	const struct tm current_date = *localtime(&current_time);
	const int t_day = current_date.tm_wday;
	const int t_mon = current_date.tm_mon;

	if (
	((tested_task.day == e_day) &&
	(tested_task.mon == e_mon)) ||

	(((int)tested_task.day == t_day) &&
	(tested_task.mon == e_mon)) ||

	(((int)tested_task.day == t_day) &&
	((int)tested_task.mon == t_mon))
	) {
		return 0;
	} else {
		return -1;
	}
}

static void
gather_info(size_t *acctl, size_t *fixedl, size_t config_l)
{
	/* count accepted tasks */
	/* count fixed tasks */
	size_t i;
	for (i=0; i<config_l; i++) {
		if (accepted_task(config_tasks[i]) == 0) {
			(*acctl)++;
			if (config_tasks[i].st[0] < (int)after_last) {
				(*fixedl)++;
			}
		}
	}
}

static Task *
create_acct(size_t acctl, size_t *tnmax, size_t config_l)
{
	/* copy accepted from config_tasks */
	/* set max task name length */
	Task *acct;
	acct = ecalloc(acctl, sizeof(Task));

	size_t i;
	size_t a = 0;

	for (i=0; i<config_l; i++){
		if (accepted_task(config_tasks[i]) == 0) {
			acct[a] = config_tasks[i];
			if (strlen(acct[a].name) > *tnmax) {
				*tnmax = strlen(acct[a].name);
			}
			a++;
		}
	}
	return acct;
}

static Task_t *
create_rett(Task *acct, size_t *acctl)
{
	/* copy accepted tasks to returned Task_t array */
	size_t i;
	Task_t *rett;
	rett = ecalloc(*acctl, sizeof(Task_t));

	for (i=0; i<*acctl; i++) {
		/* copy task name */
// 		rett[i].name = ecalloc(strlen(acct[i].name) + 1, sizeof(char));
		memcpy(rett[i].name, acct[i].name, strlen(acct[i].name) + 1);

		/* create start time */
		if (i == 0) { /* first task has no previous end time task */
			rett[i].st = adj_st(acct[i].st,0);
		} else {
			rett[i].st = adj_st(acct[i].st, rett[i-1].end);
		}

		/* create end time */
		rett[i].end = adj_end(rett[i].st, acct[i].dur);

		/* chech previous task if dynamic */
		if (i > 0){
		if ((acct[i-1].dur == (int)dyn) &&
			(acct[i].st[0] == (int)after_last)){
			die("[FAILED] %s (dyn) --> %s (dyn)\ndynamic duration must be followed by fixed task start time.", acct[i-1].name, acct[i].name);
			}
		if (acct[i-1].dur ==  (int)dyn) {
			rett[i-1].end = rett[i].st;
		}
		}

		/* last task duration */
		rett[(*acctl)-1].end = rett[0].st;
	}

	return rett;
}

static int
validate_fixed(size_t *fixedl, size_t *acctl, Task_t *rett, Task *acct)
{
	Task *fixed;
	int *btwa;
	int *btwp;
	double dfix;
	int dmin = 60 * 24;
	int dsec = dmin * 60;
// 	int tfix = 0;
	int fail = 0; /* to print all found errors */

	int fixdie = 0;
	int tdur = 0;
	size_t btwal;
	size_t i, a, x;

	fixed = ecalloc(*fixedl, sizeof(Task));
	a = 0;
	x = 0;
	btwal = (*fixedl) - 1;
	btwa = ecalloc(btwal, sizeof(int));
	btwp = ecalloc(btwal, sizeof(int));

	for (i=0; i<*acctl; i++){
		if (acct[i].st[0] < (int)after_last) { /* fixed start */
			if (a != 0){
				x++;
			}
			if (a < btwal) {
				btwa[x] = 0; /* start new btw 2 fixes */
			}
			fixed[a] = acct[i];
			a++;
		} else {  /* after last */
// 			btwa[x] += acct[i].dur;
			btwa[x] += (rett[i+1].st - rett[i].st)/60;
		}

			if (i< (*acctl)-1) {
				tdur += (rett[i+1].st - rett[i].st)/60;
			} else if (i == (*acctl)-1) {
				tdur += (rett[0].st + dsec - rett[i].st)/60;
			}
	}

	/* show fixes tasks and possible duration between them */
// 	for (i=0; i<btwal; i++) {
// 		if (i < btwal){
// 		printf("%s -> %s %d\n", fixed[i].name, fixed[i+1].name, btwa[i]);
// 		}
// 	}
// 	printf("total need fix = %d\n", tfix);

	/* calculate between each 2 fixed start times */
	/* for each 2 calculate max duration between them */
	for (i=(size_t)1; i<*fixedl; i++){
		dfix = difftime(convert_hour_min_timet(fixed[i].st),
			convert_hour_min_timet(fixed[i-1].st) +
			(fixed[i-1].dur * 60)) / 60;
		btwp[i-1] = (int)dfix;
	}

	/*
	* calculate actual sum duration of between tasks *
	* compare 2 arrays btwa & btwp fail if not equal *
	for (i=0; i<btwal; i++) {
		if (btwa[i] != btwp[i]) {
			fixdie = 1;
			printf("[\033[33mfix\033[0m] \
tasks between [%s] and [%s]: possible = %d, actual = %d\n",
				fixed[i].name,fixed[i+1].name,btwp[i], btwa[i]);
		}
	}
	*/

// 	TODO move all cases before btwa and btwp
// 	dynamic duration must be filled for checking

	/* fail cases */
	FAIL_IF (fixdie == 1,"please fix tasks duration between fixed tasks");
// 	FAIL_IF (tdur != dmax, "Total duration must be 1400 min.");

	/* calculate acctual duration */
// 	for (i=0; i<acctl; i++) {
// 		if (i< acctl-1){
// 			btwa[i] = (rett[i+1].st - rett[i].st)/60;
// 		} else if (i == acctl-1) {
// 			btwa[i] = (rett[0].st+dsec- rett[i].st)/60;
// 		}
// 		printf("%d, ", btwa[i]);
// 	}
// 	int tfix = 0;
// 		tfix += tduration;

	/* XXX VALIDATION XXX */
	/* copy fixed from accepted */
	/* count acctual duration */
	/* count total duration */

	free(btwa);
	free(btwp);
	free(fixed);

	if (fail > 0)
		return -1;

	return 0;
}

static int
validate_dyn(size_t *acctl, Task_t *rett, Task *acct)
{
	/*
	* case 0: between 2 fixes and no dynamic -> less than possible
	* case 1: dynamic duration is 0 or less
	*/

	const int dmin = 60 * 24;
	const int dsec = dmin * 60;
	time_t tduration = 0;
	int status = 0; /* to print all found errors */
	size_t i;

	for (i=0; i<*acctl; i++) {
		if (i < (*acctl)-1){
			tduration = (rett[i+1].st - rett[i].st)/60;
		} else if (i == (*acctl)-1) {
			tduration = (rett[0].st + dsec - rett[i].st)/60;
		}

		if ((tduration <= 0) ||
		(acct[i].dur != (int)dyn && (int)tduration != acct[i].dur)) {
			printf("[fix] %s duration is %ld min\n",
				rett[i].name, (long)tduration);
			status = -1;
		}
	}
	return status;
}

static void
print_t(const char print_m)
{
	Task *acct;
	Task_t *rett;
	char *st_str;
	char *end_str;
	size_t *acctl;
	size_t *fixedl;
	size_t *tnmax;
	size_t i;

	const int dmin = 60 * 24;
	const int dsec = dmin * 60;
	const time_t now_seconds = time(NULL);
	size_t config_l = LEN(config_tasks);
	time_t tduration = 0;

	acctl  = ecalloc((size_t)1, sizeof(size_t));
	fixedl = ecalloc((size_t)1, sizeof(size_t));
	tnmax  = ecalloc((size_t)1, sizeof(size_t));

	gather_info(acctl, fixedl, config_l);
	acct = create_acct(*acctl, tnmax, config_l);
	rett = create_rett(acct, acctl);

	if (validate_dyn(acctl, rett, acct) < 0)
		die("validation dynamic");

	if (validate_fixed(fixedl, acctl, rett, acct) < 0)
		die("validation fixed");

	for (i=0; i<*acctl; i++) {
		st_str = convert_timet_to_str(rett[i].st, print_m);
		end_str = convert_timet_to_str(rett[i].end, print_m);

		if (i < (*acctl)-1){
			tduration = (rett[i+1].st - rett[i].st)/60;
		} else if (i == (*acctl)-1) {
			tduration = (rett[0].st + dsec - rett[i].st)/60;
		}

		if (print_m == 'a' || print_m == 'A') {
			printf("%*s %s -> %s\t%ld\n",
				~(int)*tnmax,
				rett[i].name,
				st_str,
				end_str,
				(long)tduration);

		} else if (print_m == 'n' || print_m == 'N' || print_m == 's') {
			if (rett[i].end > now_seconds){
				printf("%*s %s -> %s\t%ld\n",
					~(int)*tnmax,
					rett[i].name,
					st_str,
					end_str,
					(long)tduration);
				if (print_m == 's') {
					free(st_str);
					free(end_str);
					break;
				}
			}
		}

		free(st_str);
		free(end_str);
	}

	free(acct);
	free(rett);
	free(acctl);
	free(fixedl);
	free(tnmax);
}

int
main(int argc, char *argv[])
{
#ifdef __OpenBSD__
	if (pledge("stdio", NULL) == -1)
		die("pledge");
#endif /* __OpenBSD__ */
	if (argc == 1) {
		print_t('s');
	} else if (argc == 2) {
		if (strcmp("-v", argv[1]) == 0){
			die("splanner-"VERSION);
		} else if (
			strcmp("-a", argv[1]) == 0 ||
			strcmp("-A", argv[1]) == 0 ||
			strcmp("-N", argv[1]) == 0 ||
			strcmp("-n", argv[1]) == 0) {
			print_t(argv[1][1]);
		} else {
			usage();
		}
	} else {
		usage();
	}
	return 0;
}
