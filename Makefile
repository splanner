# splanner
# See LICENSE file for copyright and license details.

include config.mk

SRC = splanner.c util.c
OBJ = ${SRC:.c=.o}

all: options splanner

options:
	@echo splanner build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

splanner: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f splanner ${OBJ} splanner-${VERSION}.tar.gz

dist: clean
	mkdir -p splanner-${VERSION}
	cp -R LICENSE Makefile README.md config.def.h config.mk\
		splanner.1 util.h ${SRC} splanner-${VERSION}
	tar -cf splanner-${VERSION}.tar splanner-${VERSION}
	gzip splanner-${VERSION}.tar
	rm -rf splanner-${VERSION}

install: splanner
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f splanner ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/splanner
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < splanner.1 > ${DESTDIR}${MANPREFIX}/man1/splanner.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/splanner.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/splanner\
		${DESTDIR}${MANPREFIX}/man1/splanner.1

.PHONY: all options clean dist install uninstall
